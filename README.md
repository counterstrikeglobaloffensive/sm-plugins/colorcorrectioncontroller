# Color Correction Controller
Unlike CS:S, we can't change mat_colorcorrection to 0 in CS:GO without sv_cheats 1.
This plugin allows players to use an alternative to mat_colorcorrection 0.

## Requirements
[DHooks](https://forums.alliedmods.net/showpost.php?p=2588686&postcount=589) - Experimental dynamic detour support: +detours15

## Commands
sm_cc - Open Setting Menu

## ConVar
sm_cc_enable [0/1] - Enable/Disable the feature

## Repository:
https://github.com/Xectali/ColorCorrectionController

## Preview
![Screenshot](https://i.imgur.com/qVU87Bg.jpeg)

## Updates
```
0.3 (7/24/2021)
    - Changed the method to work more properly

0.2 (7/24/2021)
    - Fix OnEntitySpawned so that the plugin can work in SM 1.10

0.1 (7/23/2021)
    - Initial release
```